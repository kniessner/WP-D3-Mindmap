<?php




 function acf_load_parent_field_choices($field)
{
    $i = 0;
    $field['choices'] = array();
   
   
    // if has rows
    if (have_rows('elements', "option")) {
       
        while (have_rows('elements', "option")) {
             the_row();
            $value = $i;
            $label = get_sub_field('name', "option");

            $field['choices'][$value] = $label;
            $i++;
        }
    }
    return $field;

}
add_filter('acf/load_field/name=element_b', 'acf_load_parent_field_choices');

function acf_load_child_field_choices($field)
{
    $i = 0;
    $field['choices'] = array();
   
   
    // if has rows
    if (have_rows('elements', "option")) {

        while (have_rows('elements', "option")) {
            the_row();
            $value = $i;
            if($i != 0 ){
                $label = get_sub_field('name', "option");
                $field['choices'][$value] = $label; 
           }
          
            $i++;
        }
    }
    return $field;

}

add_filter('acf/load_field/name=element_a', 'acf_load_child_field_choices');



function acf_load_groups_field_choices($field){
    $i = 0;
    $field['choices'] = array();

    if (have_rows('groups', "option")) {

        // while has rows
        while (have_rows('groups', "option")) {
        
            the_row();
            $color = get_sub_field('color', "option");
            $label = get_sub_field('name', "option");

            $field['choices'][$color] = $label;
            $i++;
        }

    }


    // return the field
    return $field;

}
add_filter('acf/load_field/name=group', 'acf_load_groups_field_choices');
?>