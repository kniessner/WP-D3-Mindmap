<?php
add_action('wp_ajax_categorytree', 'tlbx_ajax_callback');
add_action('wp_ajax_nopriv_categorytree', 'tlbx_ajax_callback');
function tlbx_ajax_callback()
{
    $nodes = array();
    $i = 0;
    if (have_rows('elements', "option")) {
        while (have_rows('elements', "option")) {
            the_row();
            $n = get_sub_field('name', "option");
            $o = get_sub_field('group', "option");
            $d = get_sub_field('beschreibung', "option");
            $nodes[] = [
                "id" => $i,
                "name" => $n,
                "group" => $o['label'],
                "color" => $o['value'],
                "description" => $d
            ];
            $i++;
        }
    }
    $links = array();
    if (have_rows('links', "option")) {
        while (have_rows('links', "option")) {
            the_row();
            $source = get_sub_field('element_a', "option");
            $target = get_sub_field('element_b', "option");

            $links[] = [
                "source" => intval($source['value']),
                "target" => intval($target['value'])

            ];
        }
    }
    echo json_encode(array('nodes' => $nodes, 'links' => $links), JSON_UNESCAPED_UNICODE);
    exit();
}
?>
