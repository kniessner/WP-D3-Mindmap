<?php
if (function_exists('acf_add_options_page')) {
 	// add parent
    $parent = acf_add_options_page(array(
        'page_title' => 'Toolbox Infos',
        'menu_title' => 'Toolbox Inklusion'
    ));

    // add sub page
    acf_add_options_sub_page(array(
        'page_title' => 'Mindmap Settings',
        'menu_title' => 'Mindmap',
        'parent_slug' => $parent['menu_slug'],
    ));

}

/* 
    Add css file to the admin dashboard
 */
function tlbx_admin_style(){
    wp_enqueue_style('tlbx_admin_styles', plugins_url('/css/admin_menu.css', __FILE__));
}
add_action('admin_head', 'tlbx_admin_style');

?>


